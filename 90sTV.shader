﻿Shader "Hidden/90sTV"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}

		_Intensity("Luminance Oscillation", Range(0, 50)) = 0
		
		[Header(Channel Masks)]
		_90sTV("90sTV", 2D) = "black" {}
		
		[Header(Touch Effect)]
		_touchIntensity("Touch Intensity", float) = 0.0
		_touchPositionX("Touch Position X", float) = 0.0
		_touchPositionY("Touch Position Y", float) = 0.0
		_touch("Touch Zone", 2D) = "white" {}

		[Header(Chromatic Aberration Offsets)]
		[Header(Red)]
		_RedX ("Offset X", Range(-0.5, 0.5)) = 0.0
		_RedY ("Offset Y", Range(-0.5, 0.5)) = 0.0

		[Header(Green)]
		_GreenX ("Offset X", Range(-0.5, 0.5)) = 0.0
		_GreenY ("Offset Y", Range(-0.5, 0.5)) = 0.0
		[Header(Blue)]
		_BlueX ("Offset X", Range(-0.5, 0.5)) = 0.0
		_BlueY ("Offset Y", Range(-0.5, 0.5)) = 0.0
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
			sampler2D _90sTV;

			sampler2D _touch;
			float _touchIntensity;
			float _touchPositionX;
			float _touchPositionY;

			float _RedX;
			float _RedY;
			float _GreenX;
			float _GreenY;
			float _BlueX;
			float _BlueY;
			half _Intensity;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 TV = tex2D(_90sTV, i.uv);
				fixed4 touch = tex2D(_touch, i.uv-float2(_touchPositionX, _touchPositionY));

				float despX = .007*sin(12*i.uv.y+20*_SinTime.x);
				float2 desp = float2(despX, 0);
				
				float2 red_uv = i.uv + desp + float2(_RedX, _RedY) * (TV.r + touch * _touchIntensity);
				float2 green_uv = i.uv+ desp  + float2(_GreenX, _GreenY) *(TV.r + touch * _touchIntensity);
				float2 blue_uv = i.uv + desp + float2(_BlueX, _BlueY) *(TV.r + touch * _touchIntensity);

				col.r = tex2D(_MainTex, red_uv).r;
				col.g = tex2D(_MainTex, green_uv).g;
				col.b = tex2D(_MainTex, blue_uv).b;
				
                col.rgb = (col.rgb + ((1-TV.r) * sin(_Time.y * _Intensity)) / 20) * TV.b + TV.g;
                return col;
            }
            ENDCG
        }
    }
}
