# Touch sensitive chromatic aberration

A touch sensitive shader for Unity that creates a chromatic aberration similar to a magnet near a CRT monitor.