﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiProperties : MonoBehaviour
{
    public GameObject cam;
    private float intensity;
    private bool mouse;
    private float touchPosX;
    private float touchPosY;


    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mouse = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            mouse = false;

        }
        
        if (Input.touchCount>0 || mouse==true)
        {
            
            touchPosX = (Input.GetTouch(0).position.x / Screen.currentResolution.width) - .5f;
            touchPosY = (Input.GetTouch(0).position.y / Screen.currentResolution.height) - .5f;
            intensity = 1;
            cam.GetComponent<CameraFXShader>().setTouch(intensity, touchPosX, touchPosY);
          
        }
        if (Input.touchCount==0 && mouse==false)
        {

            if (intensity > 0)
            {
                intensity = intensity-.1f;
            }
            cam.GetComponent<CameraFXShader>().setTouch(intensity, touchPosX, touchPosY);

        }
    }
}
