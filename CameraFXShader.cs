﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraFXShader : MonoBehaviour
{
    public Material FxMaterial;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setTouch(float i, float x, float y)
    {
        FxMaterial.SetFloat("_touchIntensity", i);
        FxMaterial.SetFloat("_touchPositionX", x);
        FxMaterial.SetFloat("_touchPositionY", y);
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, FxMaterial);
    }
}
